package com.subscreasy.apply.validator.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.subscreasy.apply.exception.InvalidInputException;
import com.subscreasy.apply.model.request.LoginRequest;
import com.subscreasy.apply.properties.MessageProperties;
import com.subscreasy.apply.validator.IValidator;

import lombok.extern.slf4j.Slf4j;

	
	@Slf4j
	@Component
	public class LoginRequestValidator implements IValidator<LoginRequest> {
		
		private final Logger logger = LoggerFactory.getLogger(getClass());
		
		@Autowired
		MessageProperties messages;
		

		@Override
		public void validate(LoginRequest request) {
			if (request.getEmail().isEmpty()) {
				logger.error("Email can not be empty");
				throw new InvalidInputException(messages.msgEmptyMail);
			}

			if (request.getPassword().isEmpty()) {
				logger.error("Password can not be empty");
				throw new InvalidInputException(messages.msgEmptyPassword);
			}

		}

	}



