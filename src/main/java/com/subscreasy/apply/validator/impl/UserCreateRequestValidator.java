package com.subscreasy.apply.validator.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.subscreasy.apply.exception.InvalidInputException;
import com.subscreasy.apply.model.request.UserCreateRequest;
import com.subscreasy.apply.properties.MessageProperties;
import com.subscreasy.apply.validator.IValidator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserCreateRequestValidator implements IValidator<UserCreateRequest> {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	MessageProperties messages;

	@Override
	public void validate(UserCreateRequest request) {

		String regex = "^[A-Za-z0-9+_.-]+@(.+)$";

		Pattern pattern = Pattern.compile(regex);
		if (request.getEmail().isEmpty()) {
			logger.error("CompanyId can not be empty");
			throw new InvalidInputException(messages.msgEmptyMail);
		} else {
			Matcher matcher = pattern.matcher(request.getEmail());
			if (!matcher.matches()) {
				logger.error("Mail formatı hatalı");
				throw new InvalidInputException(messages.msgMailFormatError);
			}

		}
		if (request.getName().isEmpty()) {
			logger.error("Name can not be empty");
			throw new InvalidInputException(messages.msgEmptyName);
		}
		if (request.getPassword().isEmpty()) {
			logger.error("Password can not be empty");
			throw new InvalidInputException(messages.msgEmptyPassword);
		}
		if (request.getPasswordRetry().isEmpty()) {
			logger.error("PasswordRetry can not be empty");
			throw new InvalidInputException(messages.msgEmptyPasswordRetry);
		}
		if (request.getSurname().isEmpty()) {
			logger.error("Surname can not be empty");
			throw new InvalidInputException(messages.msgEmptySurname);
		}

	}

}
