package com.subscreasy.apply.validator;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.subscreasy.apply.exception.SystemErrorException;
import com.subscreasy.apply.properties.MessageProperties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ValidatorFactory {
	
	@Autowired
	private MessageProperties messages;

	private Map<Class<?>, IValidator<?>> registry = new HashMap<>();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	public ValidatorFactory(List<IValidator<?>> validatorList) {
		validatorList.stream().forEach(validator -> register(validator));
	}

	@SuppressWarnings("unchecked")
	public <T> IValidator<T> getFor(Class<T> clazz) {
		IValidator<?> validator = this.registry.get(clazz);
		if (validator == null) {
			logger.error("Validator not found for class: " + clazz.getSimpleName());
			throw new SystemErrorException("adsas");
		}
		return (IValidator<T>) validator;
	}

	private void register(IValidator<?> validator) {
		Type[] implementedInterfaces = validator.getClass().getGenericInterfaces();
		ParameterizedType validatorInterface = (ParameterizedType) implementedInterfaces[0];
		Type[] types = validatorInterface.getActualTypeArguments();
		Class<?> clazz = (Class<?>) types[0];
		this.registry.put(clazz, validator);
	}

}
