package com.subscreasy.apply.validator;

import java.util.List;

import org.springframework.stereotype.Component;
@Component
public interface IValidator<E> {

	void validate(E element);

	default void validate(List<E> list) {
		if (list != null) {
			list.stream().forEach(e -> validate(e));
		}
	}

}
