package com.subscreasy.apply.dao;

import org.springframework.data.repository.CrudRepository;

import com.subscreasy.apply.model.dao.User;



public interface UserRepository extends CrudRepository<User, Long> {

	User findUserByEmailAndPassword(String email, String password);
}