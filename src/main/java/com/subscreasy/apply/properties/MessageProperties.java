package com.subscreasy.apply.properties;

import org.springframework.stereotype.Component;

@Component
public class MessageProperties {

	public String msgEmptyMail = "Email alanı boş bırakılamaz";
	
	public String msgEmptyName = "İsim alanı boş bırakılamaz";
	
	public String msgEmptyPassword = "Password alanı boş bırakılamaz";
	
	public String msgEmptyPasswordRetry = "PasswordRetry alanı boş bırakılamaz";
	
	public String msgEmptySurname = "Surname alanı boş bırakılamaz";
	
	public String msgWrongPasswordRetry = "Şifre ve şifre tekrarı aynı olmalı";

	public String msgSystemError = "Belirtilen class sistemde bulunamadı";
	
	public String msgMailFormatError = "Mail formatı hatalı";
	

}
