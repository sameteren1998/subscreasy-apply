package com.subscreasy.apply.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.subscreasy.apply.model.request.UserCreateRequest;
import com.subscreasy.apply.model.response.BaseResponse;
import com.subscreasy.apply.service.UserService;

@Controller(value = "usercontroller")
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private String name;
	private String surname;
	private String email;
	private String password;
	private String passwordRetry;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRetry() {
		return passwordRetry;
	}

	public void setPasswordRetry(String passwordRetry) {
		this.passwordRetry = passwordRetry;
	}

	@Autowired
	private UserService userService;


	public void createUser() {
		UserCreateRequest createUserRequest = new UserCreateRequest();
		createUserRequest.setEmail(email);
		createUserRequest.setName(name);
		createUserRequest.setPassword(password);
		createUserRequest.setPasswordRetry(passwordRetry);
		createUserRequest.setSurname(surname);
		this.logger.info("This user {}: wants to createUser: {}", createUserRequest);
		BaseResponse response = userService.userCreationProcess(createUserRequest);
		System.out.println(response.getStatusDescription());
		
	}

}
