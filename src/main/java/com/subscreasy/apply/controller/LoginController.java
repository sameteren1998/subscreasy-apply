package com.subscreasy.apply.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.subscreasy.apply.model.request.LoginRequest;
import com.subscreasy.apply.model.response.BaseResponse;
import com.subscreasy.apply.service.LoginService;

@Controller(value = "logincontroller")
public class LoginController implements Serializable  {

   private static final long serialVersionUID = -65120365720797167L;
   
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private String email;
	private String password;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Autowired
	private LoginService loginService;
	

	public void login() {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setEmail(email);
		loginRequest.setPassword(password);
		this.logger.info("This user {}: wants to login: {}", loginRequest);
		BaseResponse response = loginService.login(loginRequest);
		System.out.println(response.getStatusDescription());
	
	}
	

	@PostConstruct
	public void init() {
		
	}
	


}
