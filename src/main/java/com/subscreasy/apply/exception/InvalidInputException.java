package com.subscreasy.apply.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.subscreasy.apply.type.ErrorType;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidInputException extends UserServiceException {

	private static final long serialVersionUID = 1L;

	public InvalidInputException(String message) {
		super(ErrorType.VALIDATION_ERROR.getResultCode(), message);
	}

}
