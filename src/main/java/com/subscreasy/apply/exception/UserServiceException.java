package com.subscreasy.apply.exception;

import com.subscreasy.apply.model.response.BaseResponse;
import com.subscreasy.apply.type.StatusType;

public abstract class UserServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String statusCode;
	private final String statusDescription;

	public UserServiceException(String statusCode, String statusDescription) {
		this.statusCode = statusCode;
		this.statusDescription = statusDescription;
	}

	public BaseResponse getResponse() {
		BaseResponse response = new BaseResponse();
		response.setStatus(StatusType.FAIL.toString());
		response.setStatusCode(this.statusCode);
		response.setStatusDescription(this.statusDescription);
		return response;
	}

}
