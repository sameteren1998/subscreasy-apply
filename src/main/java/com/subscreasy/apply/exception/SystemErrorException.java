package com.subscreasy.apply.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.subscreasy.apply.type.ErrorType;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class SystemErrorException extends UserServiceException {

	private static final long serialVersionUID = 1L;

	public SystemErrorException(String message) {
		super(ErrorType.GENERAL_SYSTEM_ERROR.getResultCode(), message);
	}
}
