package com.subscreasy.apply.service;

import com.subscreasy.apply.model.request.LoginRequest;
import com.subscreasy.apply.model.response.BaseResponse;

public interface LoginService {
	
	BaseResponse login(LoginRequest login);
	
}
