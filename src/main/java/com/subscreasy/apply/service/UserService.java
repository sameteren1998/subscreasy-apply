package com.subscreasy.apply.service;

import com.subscreasy.apply.model.request.UserCreateRequest;
import com.subscreasy.apply.model.response.BaseResponse;

public interface UserService {
	
	BaseResponse userCreationProcess(UserCreateRequest createUserRequest);
	
}
