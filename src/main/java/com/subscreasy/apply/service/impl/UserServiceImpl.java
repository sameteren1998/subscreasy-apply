package com.subscreasy.apply.service.impl;


import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subscreasy.apply.dao.UserRepository;
import com.subscreasy.apply.model.dao.User;
import com.subscreasy.apply.model.request.UserCreateRequest;
import com.subscreasy.apply.model.response.BaseResponse;
import com.subscreasy.apply.service.UserService;
import com.subscreasy.apply.validator.ValidatorFactory;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	public ValidatorFactory validatorFactory;

	@Autowired
	private UserRepository repository;

	@Override
	public BaseResponse userCreationProcess(UserCreateRequest createUserRequest) {

		this.validatorFactory.getFor(UserCreateRequest.class).validate(createUserRequest);
		User user = new User();
		user.setEmail(createUserRequest.getEmail());
		user.setName(createUserRequest.getName());
		user.setSurname(createUserRequest.getSurname());
		user.setPassword(createUserRequest.getPassword());
//		user.setId(UUID.randomUUID().toString());
		try {
			repository.save(user);
		} catch (Exception e) {
			BaseResponse response = new BaseResponse();
			response.setStatus("FAIL");
			response.setStatusCode("404");
			response.setStatusDescription("User oluşturulurken hata");
			return response;
		}
		BaseResponse response = new BaseResponse();
		response.setStatus("SUCCESS");
		response.setStatusCode("202");
		response.setStatusDescription("User oluşturuldu");
		return response;
	}

}
