package com.subscreasy.apply.service.impl;


import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subscreasy.apply.dao.UserRepository;
import com.subscreasy.apply.model.dao.User;
import com.subscreasy.apply.model.request.LoginRequest;
import com.subscreasy.apply.model.request.UserCreateRequest;
import com.subscreasy.apply.model.response.BaseResponse;
import com.subscreasy.apply.service.LoginService;
import com.subscreasy.apply.service.UserService;
import com.subscreasy.apply.validator.ValidatorFactory;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	public ValidatorFactory validatorFactory;

	@Autowired
	private UserRepository repository;
	
	@Override
	public BaseResponse login(LoginRequest login) {
		
		this.validatorFactory.getFor(LoginRequest.class).validate(login);
		
		BaseResponse response = new BaseResponse();
		User user = repository.findUserByEmailAndPassword(login.getEmail(), login.getPassword());
		if(user != null)
		{
			response.setStatus("SUCCESS");
			response.setStatusCode("202");
			response.setStatusDescription("Giriş Başarılı");
			return response;
		}
		response.setStatus("FAIL");
		response.setStatusCode("404");
		response.setStatusDescription("Giriş Başarısız");
		return response;
	}

	

}
